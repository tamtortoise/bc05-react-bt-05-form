import React, { Component } from "react";
import { connect } from "react-redux";
import {THEM_SINH_VIEN, UPDATE_SINH_VIEN} from './redux/constant/constant'


class FormInput extends Component {
  // tạo state mới lấy dữ liệu trong form truyền lên store
  state = {
    svNew: [
      {
        id: '',
        hoTen: '',
        phone: '',
        email: '',
      },
    ],
    errors: {
      id: '',
      hoTen: '',
      phone: '',
      email: '',
    },
    valid: false,
    svListCheck: [],
  };

  handleGetInput = (event) => {
    // Tạo function bóc tách value từ event.target. Ngoài ra thêm thuộc tính name vào trong form's input bên dưới, để có thể gọi hàm dùng chung cho tất cả input.
    let { value, name, type, pattern } = event.target;
    let errorMsg = '';
    // CHECK ALL INPUT FORMAT
    // CASE: NULL
    if (value.trim() === ''){ // <-- nếu ô input rỗng, ko tính khoảng trắng đầu cuối (trim())
      errorMsg = name + ' không được để trống!';
    }
    // CASE: NOT NULL
    // Tạo function check riêng cho ID vì có trường hợp không để trùng ID
    // Kham khảo Anh Trần
    if (value.trim() !== '' && (type === 'email' || type === 'text')){
      if (name === "id"){
        const regex = new RegExp(pattern);
        if (!regex.test(value)){
          errorMsg = name + ' không đúng định dạng hoặc yêu cầu!'
        }
        let idValue = {[name]:value}
        let svListCheck = this.props.svList.map(item => item.id)
        console.log('svListCheck',svListCheck); //-> output là array với dãy số ID [1,2,3,4,5]
        // tạo vòng lặp so sánh nếu từng phần tử trong array trùng với giá trị ID nhập vào (dùng chung format Number để dấu so sánh === hoạt động) => hiện lỗi trùng
        svListCheck.forEach(element => {
          if (Number(element) === Number(idValue.id)){
            errorMsg = name + ' trùng với mã SV đã khởi tạo!'
          }
        })
      }
      // check cho các input còn lại (chỉ còn check format valid)
      else{
        const regex = new RegExp(pattern);
        if (!regex.test(value)){
          errorMsg = name + ' không đúng định dạng hoặc yêu cầu!'
        }
      }
    } 
    // update value/error
    let svNew = { ...this.state.svNew,[name]:value};
    let errors = {...this.state.errors,[name]:errorMsg}
    // reset state
    this.setState(
      {
        ...this.state,
        svNew: svNew,
        errors: errors,
      },
      () => {
        console.log(this.state);
        this.checkValidation();
      }
    );
  };
  // Tạo set condition of validation to enable Add Button
  checkValidation =()=>{
    let valid = true;
    // Check erros
    for (let item in this.state.errors){
      // nếu như 1 trong các input có lỗi (errorMsg !== '')
      if (this.state.errors[item] !== ''){
        valid = false;
      }
    }
    // kiểm tra rỗng
    if (valid === true){
      //  nếu valid = true, nhưng giá trị nhập vào của input là rỗng thì vần set valid = false
      for (let item in this.state.svNew){
        if (this.state.svNew[item] === ''){
          valid = false;
        }
      }
    }
    // cập nhật state cho valid
    this.setState({valid:valid})
  }

  render() {
    return (
      <div>
        <div className="row pb-5">
          <div className="col-12">
            <h2 className="bg-info text-light py-3 pl-2 text-left">
              Thông tin sinh viên
            </h2>
          </div>
          <div className="form-group col-6">
            <p className="text-left" style={{fontWeight: '600'}}>Mã SV</p>
            <input
              onChange={this.handleGetInput}
              type="text" pattern="^\d+$"
              className="form-control"
              name="id" placeholder="Mã số"
              value={this.state.svNew.id || ''}
              required
            />
            <span className="text-danger">{this.state.errors.id}</span>
          </div>
          <div className="form-group col-6">
            <p className="text-left" style={{fontWeight: '600'}}>Họ tên</p>
            <input
              onChange={this.handleGetInput}
              type="text" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*)[a-zA-Z\s]{5,}$"
              className="form-control"
              name="hoTen" placeholder="Nhập tên không dấu nhá !!!"
              value={this.state.svNew.hoTen || ''}
              required
            />
            <span className="text-danger">{this.state.errors.hoTen}</span>
          </div>
          <div className="form-group col-6">
            <p className="text-left" style={{fontWeight: '600'}}>Số điện thoại</p>
            <input
              onChange={this.handleGetInput}
              type="text" pattern="^(0|\+84)(\s|\.)?((3[2-9])|(5[689])|(7[06-9])|(8[1-689])|(9[0-46-9]))(\d)(\s|\.)?(\d{3})(\s|\.)?(\d{3})$"
              className="form-control"
              name="phone" placeholder="Điền số đt"
              value={this.state.svNew.phone || ''}
              required
            />
            <span className="text-danger">{this.state.errors.phone}</span>
          </div>
          <div className="form-group col-6">
            <p className="text-left" style={{fontWeight: '600'}}>Email</p>
            <input
              onChange={this.handleGetInput}
              type="email" pattern="^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$"
              className="form-control"
              name="email" placeholder="gmail/yahoo/skype mail"
              value={this.state.svNew.email || ''}
              required
            />
            <span className="text-danger">{this.state.errors.email}</span>
          </div>
          <div className="col-12 text-right mt-1">

            {/* Kham khảo Anh Trần */}
            <br />
            {/* set điều kiện ẩn hiện nút thêm */}
            {this.state.valid ? <button onClick={() => {this.props.handleAddSV(this.state.svNew);}} className="btn btn-primary py-2 text-left mr-2">Thêm sinh viên</button> : <button onClick={() => {this.props.handleAddSV(this.state.svNew);}} className="btn btn-primary py-2 text-left mr-2" disabled>Thêm sinh viên</button>}

            {/* set điều kiện ẩn hiện nút update */}
            {this.state.valid ? <button onClick={() => {this.props.handleUpdateSV(this.state.svNew);}} className="btn btn-warning py-2 text-left">Cập nhật sinh viên</button> : <button onClick={() => {this.props.handleUpdateSV(this.state.svNew);}} className="btn btn-warning py-2 text-left" disabled>Cập nhật sinh viên</button>}
          </div>
        </div>
      </div>
    );
  }
  // Ghi chú: chạy sau render, nhưng đây là lifecycle trả về props cũ và state cũ của component trước khi render
  componentDidUpdate(prevProps, prevState){
    // Tránh bị lập vô tận ta nên => (setState rồi render lại, rồi lại setState...), chúng ta cần lệnh so sánh nếu props trước đó (svEdit trước mà khác với svEdit hiện tại thì mới setState)
    if (prevProps.svEdited.id !== this.props.svEdited.id){
        this.setState({
            svNew: this.props.svEdited
        })
    } 

  }
}

let mapStateToProps = (state) => {
  return {
    svList: state.formReducer.svList,
    svEdited: state.formReducer.svEdited,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleAddSV: (sinhVien) => {
      const action = {
        type: THEM_SINH_VIEN,
        sinhVien,
      };
      dispatch(action);
    },
    handleUpdateSV: (sinhVien) => {
        const action = {
          type: UPDATE_SINH_VIEN,
          sinhVien,
        };
        dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FormInput);
