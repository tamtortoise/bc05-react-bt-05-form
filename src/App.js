import './App.css';
import FormStoreReducer from './ExForm/FormStoreReducer';

function App() {
  return (
    <div className="App">
      <FormStoreReducer/>
    </div>
  );
}

export default App;
